package com.example.ejercicioformulario

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.ejercicioformulario.ui.theme.EjercicioFormularioTheme


class HuertoActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EjercicioFormularioTheme() {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Huerto1()
                }
            }
        }
    }


    @Composable
    fun Huerto1() {
        var imagen = painterResource(id = R.drawable.huerto)
        var imagen_planta = painterResource(id = R.drawable.huerto_planta)
        var imagen_fruta = painterResource(id = R.drawable.huerto_frutas)

        var num1 by remember {
            mutableStateOf(1)
        };
        var acum by remember {
            mutableStateOf(0)
        };
        var num = 0


        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            if (num1 == 1) {
                Image(
                    painter = imagen,
                    contentDescription = "Huerto simple",
                    modifier = Modifier.fillMaxSize()

                )
            } else if (num1 == 2) {
                Image(
                    painter = imagen_planta,
                    contentDescription = "Huerto con magia",
                    modifier = Modifier.fillMaxSize()
                )
            } else if (num1 == 3) {
                Image(
                    painter = imagen_fruta,
                    contentDescription = "Huerto con fruta",
                    modifier = Modifier.fillMaxSize()
                )
            } else if (num1 > 3) {
                num1 = 1;
            }


        }
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Bottom,

            ) {
            Button(
                onClick = {
                    num1++
                    if (num1 == 3) {
                        num = (1..5).random()
                        acum += num
                    }

                }
            )
            {
                Text(text = "Cambiar imagen")


            }
            Text(
                text = acum.toString(),
                color = Color.DarkGray,

                )

        }


    }

    @Preview(showBackground = true)
    @Composable
    fun GreetingPreview1() {
        EjercicioFormularioTheme {
            Huerto1()
        }
    }
}